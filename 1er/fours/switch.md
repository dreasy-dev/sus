## First

```fortran
en
conf t
vlan 10
name flic
e
vlan 50
name enquet
e
vlan 100
name sergent-cap
e
vlan 666
name cameras
e
```

full flic+ cam

```fortran
en
conf t
interface FastEthernet 0/1 
switchport mode trunk
switchport trunk allowed vlan 10,666
no sh 
ex
interface FastEthernet 0/2
switchport mode acc
switchport acc vlan 666
no sh 
ex
interface FastEthernet 0/3
switchport mode acc
switchport acc vlan 10
no sh 
ex
int fa 0/24
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
int fa 0/23
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
int fa 0/22
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
int fa 0/21 
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
int fa 0/20
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
int fa 0/19
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
int fa 0/18
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
```
## Secondary

flic + cam

```fortran
en
conf t
interface FastEthernet 0/1 
switchport mode trunk
switchport trunk allowed vlan 10,666
no sh 
ex
interface FastEthernet 0/2
switchport mode acc
switchport acc vlan 666
no sh 
ex
interface FastEthernet 0/3
switchport mode acc
switchport acc vlan 10
no sh 
ex
int fa 0/24
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
int fa 0/23
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
int fa 0/22
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
int fa 0/21 
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
int fa 0/20
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
int fa 0/19
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
int fa 0/18
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
```
## Third

sergent+cam

```fortran
en
conf t
interface FastEthernet 0/1 
switchport mode trunk
switchport trunk allowed vlan 10,50,100,666
no sh 
ex
interface FastEthernet 0/2
switchport mode acc
switchport acc vlan 666
no sh 
ex
interface FastEthernet 0/3
switchport mode acc
switchport acc vlan 100
no sh 
ex
interface FastEthernet 0/4
switchport mode acc
switchport acc vlan 50
no sh 
ex
interface FastEthernet 0/5
switchport mode acc
switchport acc vlan 50
no sh 
ex
interface FastEthernet 0/6
switchport mode acc
switchport acc vlan 50
no sh 
ex
interface FastEthernet 0/7
switchport mode acc
switchport acc vlan 50
no sh 
ex
interface FastEthernet 0/8
switchport mode acc
switchport acc vlan 50
no sh 
ex
interface FastEthernet 0/9
switchport mode acc
switchport acc vlan 100
no sh 
ex
interface FastEthernet 0/10
switchport mode acc
switchport acc vlan 100
no sh 
ex
int fa 0/24
sw mode trunk
sw trunk allowed vlan 10,50,100,666
spanning-tree vlan 10,50,100,666 priority 24576
int fa 0/23
sw mode trunk
sw trunk allowed vlan 10,50,100,666
spanning-tree vlan 10,50,100,666 priority 24576
int fa 0/22
sw mode trunk
sw trunk allowed vlan 10,50,100,666
spanning-tree vlan 10,50,100,666 priority 24576
int fa 0/21 
sw mode trunk
sw trunk allowed vlan 10,50,100,666
spanning-tree vlan 10,50,100,666 priority 24576
int fa 0/20
sw mode trunk
sw trunk allowed vlan 10,50,100,666
spanning-tree vlan 10,50,100,666 priority 24576
int fa 0/19
sw mode trunk
sw trunk allowed vlan 10,50,100,666
spanning-tree vlan 10,50,100,666 priority 24576
int fa 0/18
sw mode trunk
sw trunk allowed vlan 10,50,100,666
spanning-tree vlan 10,50,100,666 priority 24576
```
## Four

flic + cam

```fortran
en
conf t
interface FastEthernet 0/1 
switchport mode trunk
switchport trunk allowed vlan 10,666
no sh 
ex
interface FastEthernet 0/2
switchport mode acc
switchport acc vlan 666
no sh 
ex
interface FastEthernet 0/3
switchport mode acc
switchport acc vlan 10
no sh 
ex
int fa 0/24
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
int fa 0/23
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
int fa 0/22
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
int fa 0/21 
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
int fa 0/20
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
int fa 0/19
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
int fa 0/18
sw mode trunk
sw trunk allowed vlan 10,100,666
spanning-tree vlan 10,100,666 priority 24576
```
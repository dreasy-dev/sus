## **Conf switch**

```fortran
en
conf t
vlan 10
name flic
e
vlan 15
name archive 
e
vlan 20
name accueil 
e
vlan 25
name imprim
e
vlan 50
name enquet
e
vlan 69
name info
e
vlan 100
name sergent-cap
e
vlan 404
name serveur
e
vlan 666
name cameras
e
vlan 999
name secu
e
```

```fortran
en
conf t
interface gig 0/1
switchport mode trunk
switchport trunk allowed vlan 10,15,20,25,50,69,100,404,666,999
no sh
exit
interface range fastEthernet 0/1 - 2
channel-group 4 mode active
exit
interface range fastEthernet 0/3 - 4
channel-group 5 mode active
exit
interface range fastEthernet 0/5 - 6
channel-group 1 mode active
exit
interface range fastEthernet 0/7 - 8
channel-group 2 mode active
exit
interface port-channel 4
switchport mode trunk
switchport trunk allowed vlan 10,15,20,25,50,69,100,404,666,999
no shutdown
exit
interface port-channel 5
switchport mode trunk
switchport trunk allowed vlan 10,15,20,25,50,69,100,404,666,999
no shutdown
exit
interface port-channel 1
switchport mode trunk
switchport trunk allowed vlan 10,15,20,25,50,69,100,404,666,999
no shutdown
exit
interface port-channel 2
switchport mode trunk
switchport trunk allowed vlan 10,15,20,25,50,69,100,404,666,999
no shutdown
exit
```

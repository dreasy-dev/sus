## **Conf switch**
s
```fortran
en
conf t
vlan 10
name flic
e
vlan 15
name archive 
e
vlan 20
name accueil 
e
vlan 25
name imprim
e
vlan 50
name enquet
e
vlan 69
name info
e
vlan 100
name sergent-cap
e
vlan 404
name serveur
e
vlan 666
name cameras
e
vlan 999
name secu
e
```

```fortran
en
conf t
interface range fastEthernet 0/1 - 2
channel-group 5 mode active
exit
interface port-channel 5
switchport mode trunk
switchport trunk allowed vlan 15,20,25,666
no shutdown
exit
interface FastEthernet 0/3
switchport mode access
switchport access vlan 15
no sh
exit
interface FastEthernet 0/4
switchport mode access
switchport access vlan 15
no sh
exit
interface FastEthernet 0/5
switchport mode access
switchport access vlan 15
no sh
exit
interface FastEthernet 0/6
switchport mode access
switchport access vlan 20
no sh
exit
interface FastEthernet 0/7
switchport mode access
switchport access vlan 25
no sh
exit
interface FastEthernet 0/8
switchport mode access
switchport access vlan 666
no sh
exit
```

## **Conf routeur**



```fortran
flic : 86  /25      vlan 10
archives : 1 /29    vlan 15
accueil : 3   /29   vlan 20
imprim : 1 /29      vlan 25
enqueteurs : 10 /28 vlan 50
info : 7 /28        vlan 69
sergent : 4 /29     vlan 100
serveur : 6 /28     vlan 404
camera : 31 /26     vlan 666
secu : 1 /30        vlan 999
```


```fortran
en
conf t
int gig 0/1
ip add 10.0.0.2 255.255.255.0
ip nat inside
no shutdown
ex
access-list 1 permit 10.0.0.2 0.0.0.255
ip nat inside source list 1 interface gig 0/1 overload
interface GigabitEthernet0/0.1
encapsulation dot1Q 10
ip address 192.168.10.1 255.255.255.128
ip helper-addres 192.168.250.10
no sh
ex
interface GigabitEthernet0/0.15
encapsulation dot1Q 15
ip address 192.168.15.1 255.255.255.248
ip helper-addres 192.168.250.10
no sh
ex
interface GigabitEthernet0/0.20
encapsulation dot1Q 20
ip address 192.168.20.1 255.255.255.248
ip helper-addres 192.168.250.10
no sh
ex
interface GigabitEthernet0/0.25
encapsulation dot1Q 25
ip address 192.168.25.1 255.255.255.248
ip helper-addres 192.168.250.10
no sh
ex
interface GigabitEthernet0/0.5
encapsulation dot1Q 50
ip address 192.168.50.1 255.255.255.240
ip helper-addres 192.168.250.10
no sh
ex
interface GigabitEthernet0/0.69
encapsulation dot1Q 69
ip address 192.168.69.1 255.255.255.240
ip helper-addres 192.168.250.10
no sh
ex
interface GigabitEthernet0/0.101
encapsulation dot1Q 100
ip address 192.168.100.1 255.255.255.248
ip helper-addres 192.168.250.10
no sh
ex
interface GigabitEthernet0/0.40
encapsulation dot1Q 404
ip address 192.168.250.1 255.255.255.240
ip helper-addres 192.168.250.10
no sh
ex
interface GigabitEthernet0/0.66
encapsulation dot1Q 666
ip address 192.168.150.1 255.255.255.192
ip helper-addres 192.168.250.10
no sh
ex
interface GigabitEthernet0/0.9
encapsulation dot1Q 999
ip address 192.168.200.1 255.255.255.252
ip helper-addres 192.168.250.10
no sh
ex
```
## **Conf switch**

```fortran
en
conf t
vlan 10
name flic
e
vlan 15
name archive 
e
vlan 20
name accueil 
e
vlan 25
name imprim
e
vlan 50
name enquet
e
vlan 69
name info
e
vlan 100
name sergent-cap
e
vlan 404
name serveur
e
vlan 666
name cameras
e
vlan 999
name secu
e
```

```fortran
en
conf t
interface range fastEthernet 0/1 - 2
channel-group 2 mode active
exit
interface port-channel 2
switchport mode trunk
switchport trunk allowed vlan 10,50,69,100,404,666
no shutdown
exit
interface FastEthernet 0/3
switchport mode trunk
switchport trunk allowed vlan 10,50,69,100,666
no sh 
exit
interface FastEthernet 0/4
switchport mode trunk
switchport trunk allowed vlan 10,666
no sh  
exit
interface FastEthernet 0/5
switchport mode trunk
switchport trunk allowed vlan 10,666
no sh  
exit
interface FastEthernet 0/6
switchport mode trunk
switchport trunk allowed vlan 10,666
no sh  
exit
interface range fastEthernet 0/23 - 24
channel-group 8 mode active
exit
interface port-channel 8
switchport mode trunk
switchport trunk allowed vlan 69,404,666
no shutdown 
exit
interface range fastEthernet 0/23 - 24
channel-group 3 mode active
exit
interface port-channel 3
switchport mode trunk
switchport trunk allowed vlan 69,404,666
no shutdown 
exit
int fa 0/22
sw mode trunk
sw trunk allowed vlan 10,50,69,100,666
spanning-tree vlan 10,50,69,100,666 priority 24576
int fa 0/21 
sw mode trunk
sw trunk allowed vlan 10,50,69,100,666
spanning-tree vlan 10,50,69,100,666 priority 24576
```

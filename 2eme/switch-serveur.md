## **Conf switch**

```fortran
en
conf t
vlan 69
name info
e
vlan 404
name serveur
e
vlan 666
name cameras
e

```

```fortran
en
conf t
exit
interface FastEthernet 0/2
switchport mode access
switchport access vlan 666
no sh  
exit
interface FastEthernet 0/3
switchport mode access
switchport access vlan 404
no sh 
exit
interface FastEthernet 0/4
switchport mode access
switchport access vlan 404
no sh  
exit
interface FastEthernet 0/5
switchport mode access
switchport access  vlan 404
no sh  
exit
interface FastEthernet 0/6
switchport mode access
switchport access vlan 404
no sh  
exit
interface FastEthernet 0/7
switchport mode access
switchport access vlan 404
no sh
exit
interface FastEthernet 0/8
switchport mode access
switchport access vlan 404
no sh 
exit
interface range fastEthernet 0/23 - 24
channel-group 3 mode active
exit
interface port-channel 3
switchport mode trunk
switchport trunk allowed vlan 69,404,666
no shutdown 
exit
```

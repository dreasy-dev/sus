## *sus*

# TP FINAL 

---

### Sous-sol

- [Configuration des switches](./Sous-sol/switch.md)

### Rez de chaussée

- [Configuration du routeur](./rez-chausser/router/conf.md#conf-routeur)
- [Configuration de switch central](./rez-chausser/switch-base/Switch.md#conf-switch)
- [Configuration des switches](./rez-chausser/Switchs.md#conf-switch)

### 1er étage

- [Configuration switch coeur](./1er/switch.md)
- [Configuration switch n°1](./1er/fours/switch.md#first)
- [Configuration switch n°2](./1er/fours/switch.md#secondary)
- [Configuration switch n°3](./1er/fours/switch.md#third)
- [Configuration switch n°4](./1er/fours/switch.md#four)

### 2nd étage

- [Configuration switch coeur](./2eme/switch.md)
- [Configuration switch serveur](./2eme/switch-serveur.md)
- [Configuration switch n°1](./2eme/fours/switch.md#first)
- [Configuration switch n°2](./2eme/fours/switch.md#secondary)
- [Configuration switch n°3](./2eme/fours/switch.md#third)
- [Configuration switch n°4](./2eme/fours/switch.md#four)
